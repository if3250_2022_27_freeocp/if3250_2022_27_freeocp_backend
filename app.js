const express = require('express')
const cors = require('cors')
const path = require('path');
const jwt = require('jsonwebtoken')
const db = require('./db/conn')

const app = express()
const port = 8000

const User = require('./models/users')

const dotenv = require('dotenv')
dotenv.config()

app.use(cors())
app.use(express.json())
app.use(
  express.urlencoded({
    extended: true,
  })
)

// Check if token is authorized
const verifyJWT = (req, res, next) => {
	const token = req.headers["x-access-token"]
  
	if (!token) {
	  res.status(401).json({ auth: false, message: "You don't have a token" })
	}
	else {
		jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
			if (err) {
				res.status(401).json({ auth: false, message: "You fail to authenticate" })
			}
			else {
				res.locals.id = decoded.id
				User.findById(res.locals.id, (err, result) => {
					res.locals.is_curator = result['is_curator']
					res.locals.is_admin = result['is_admin']
					next()
				})
			}
		})
	}
}

const verifyJWTNotMust = (req, res, next) => {
	const token = req.headers["x-access-token"]


	if (token) {
		jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {
			if (decoded) {
				res.locals.id = decoded.id
				User.findById(res.locals.id, (err, result) => {
					res.locals.is_curator = result['is_curator']
					next()
				})
			}else {
				next()
			}
		})
	}
	else {
		next()
	}
}

app.get("/is-auth", verifyJWT, (req, res, next) => {
	res.status(200).json({
		auth: true,
		message: "User is authenticated",
		is_curator: res.locals.is_curator,
		is_admin: res.locals.is_admin
	})
})

// Calling Routes
var loginRoutes = require('./routes/login.routes')
var logoutRoutes = require('./routes/logout.routes')
var registerRoutes = require('./routes/register.routes')
var profileRoutes = require('./routes/profile.routes')
var searchRoutes = require('./routes/search.routes')
var courseManagementRoutes = require('./routes/course.management.routes')
var courseRoutes = require('./routes/course.routes')
var assessmentRoutes = require('./routes/assessment.routes')
var curatorRoutes = require('./routes/curator.routes')
var gradingRoutes = require('./routes/grading.routes')
var enrollRoutes = require('./routes/enroll.routes')
var adminDashboardRoutes = require('./routes/admin.dashboard.routes')
var progressDataRoutes = require('./routes/progress.data.routes')
var uploadImageRoutes = require('./routes/upload.image.routes')
var deleteAllRoutes = require('./routes/deleteall.routes')

// Use Endpoints for Routes
app.get('/', (req, res) => {
  res.send('Welcome to FREEOCP Backend Server!')
})
app.use('/login', loginRoutes)
app.use('/logout', logoutRoutes)
app.use('/register', registerRoutes)
app.use('/user', verifyJWT, profileRoutes)
app.use('/search', searchRoutes)
app.use('/course-management',verifyJWT, courseManagementRoutes)
app.use('/course', verifyJWTNotMust, courseRoutes)
app.use('/assessment', verifyJWT ,assessmentRoutes)
app.use('/curator', verifyJWT, curatorRoutes)
app.use('/grading', verifyJWT, gradingRoutes)
app.use('/enroll', verifyJWT, enrollRoutes)
app.use('/admin-dashboard', verifyJWT, adminDashboardRoutes)
app.use('/progress-data', verifyJWTNotMust, progressDataRoutes)
app.use('/upload-image', verifyJWT, uploadImageRoutes)
app.use('/delete-all', verifyJWT, deleteAllRoutes)

// get images
app.get('/profpic/:profpic_name', (req, res) => {
	res.sendFile(path.join(__dirname, 'img/profpic', req.params.profpic_name))
})

app.get('/coursepic/:coursepic_name', (req, res) => {
	res.sendFile(path.join(__dirname, 'img/coursepic', req.params.coursepic_name))
})


// Handling errors and undefined endpoints
app.use((req, res) => {
	res.status(404).json({
		message: 'Route Not Found',
	})
})
  
app.use((err, req, res, next) => {
	res.status(500).json({
		error: err.message,
	})
})

// Connecting to db and listening on port 8000
db.connectToDB(() => {
	app.listen(process.env.PORT || 8000)
	console.log(`App listening on port ${port}`)
})

module.exports = app