const dotenv = require('dotenv')
dotenv.config()

const mongoose = require('mongoose')
const connectionString = process.env.ATLAS_URI;

const connectToDB = (callback) => {
	mongoose.connect(connectionString, {
		useNewUrlParser: true,
		useUnifiedTopology: true,
	}).then(() => {
		console.log("Successfully connected to MongoDB.")
		callback()
	}).catch((err) => {
		console.log(err)
	})
}

const getMongoose = () => {
	return mongoose;
}

module.exports = {
  connectToDB,
  getMongoose,
};