const ObjectId = require('mongodb').ObjectId
const User = require('../models/users')
const TakesCourse = require('../models/takescourse')
const Course = require('../models/course')
const Material = require('../models/material')
const Chapter = require('../models/chapter')

const bcrypt = require('bcrypt')

// Send all the data of the user with the username except the password
const getProfile = (req, res, next) => {
    const user_id = res.locals.id

    if (!ObjectId.isValid(user_id)) {
        res.status(404).send({
            message: 'User not found'
        })
        return
    }

    User.findById(user_id,
         (err, user) => {
        if (err) {
            res.status(500).send({
                message: 'Internal server error',
                err: err
            })
            return
        }

        if (!user) {
            res.status(404).send({
                message: 'User not found'
            })
            return
        }

        res.status(200).send({
            email: user.email,
            full_name: user.full_name,
            date_of_birth: user.date_of_birth,
            is_curator: user.is_curator,
            profile_picture: user.profile_picture,
            institution: user.institution,
		    description: user.description
        })
    })
}

// Update the user data if the user is the same as the one who is logged in
const updateProfile = (req, res, next) => {
    const user_id = res.locals.id
    const { full_name, date_of_birth, profile_picture, institution, description } = req.body

    User.findById(user_id,
        (err, user) => {
        if (err) {
            res.status(500).send({
                message: 'Internal server error',
                err: err
            })
            return
        }

        if (!user) {
            res.status(404).send({
                message: 'User not found'
            })
            return
        }

        // Update
        user.full_name = full_name ? full_name  : user.full_name
        user.date_of_birth = date_of_birth ? date_of_birth : user.date_of_birth
        user.profile_picture = profile_picture ? profile_picture : user.profile_picture
        user.institution = institution ? institution : user.institution,
		user.description = description ? description : user.description

        // Save
        user.save((err, user) => {
            if (err) {
                res.status(500).send({
                    message: 'Internal server error',
                    err: err
                })
                return
            }

            res.status(200).send({
                message: 'User updated successfully'
            })
        }
        )
    })
}

// Change password if the user is the same as the one who is logged in
const changePassword = (req, res, next) => {
    const user_id = res.locals.id
    const { password } = req.body

    User.findById(user_id,
        (err, user) => {
        if (err) {
            return res.status(500).send({
                message: 'Internal server error',
                err: err
            })
        }

        if (!user) {
            return res.status(404).send({
                message: 'User not found'
            })
        }

        // Check if the password length is more than 8 characters
        if (password.length < 8) {
            return res.status(400).send({
                message: 'Password must be at least 8 characters'
            })
        }

        // Check if password only consist of alphabets, underscore, numbers, hyphen, and period
        if (!password.match(/^[a-zA-Z0-9_\-\.]+$/)) {
            return res.status(400).send({
                message: 'Password must only consist of alphabets, underscore, numbers, hyphen, and period'
            })
        }

        // Check if password consist of at least one number, one uppercase letter, and one lowercase letter
        if (!password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)) {
            return res.status(400).send({
                message: 'Password must consist of at least one number, one uppercase letter, and one lowercase letter'
            })
        }

        const salt = bcrypt.genSaltSync()
        // Change the password
        user.password = password ? bcrypt.hashSync(password, salt) : user.password 

        // Save
        user.save((err, user) => {
            if (err) {
                return res.status(500).send({
                    message: 'Internal server error',
                    err: err
                })
            }

            return res.status(200).send({
                message: 'Password changed successfully'
            })
        }
        )
    })
}

const getCoursesProgress = async (req, res, next) => {
    // Example of the response:
    //
    // {
    //  count: 5,
    //  data:  
    // [{
    //     course_id: '5c9b8f8f9c9d440000c8f8f8',
    //     thumbnail: 'https://atariwiki.org/wiki/attach/6502%20Assembly%20Code/6502%20Assembly%20Code%20in%20Sublime%20Text.jpg',
    //     title: 'Dasar Assembly',
    //     topic: 'Informatika',
    //     creator: 'Randy Zakya Suchrady Alter.',
    //     progress: 90,
    //     material_done: 9,
    //     total_material: 10,
    //     assessment_done: 9,
    //     total_assessment: 10,
    //     chapter_done: 0,
    //     total_chapter: 1,
    //     start_date: new Date(2022, 1, 2),
    //     finish_date: null,
    //     },...]
    // }
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    try {
        const takescourses = await TakesCourse.find({ user_id: user_id })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        if (!takescourses) {
            return res.status(404).send({
                message: 'User not found'
            })
        }
        const takescoursecount = await TakesCourse.countDocuments({ user_id: user_id })
        const courses_id = takescourses.map(takescourse => takescourse.course_id)
        const courses = await Course.find({ _id: { $in: courses_id } })
        if (!courses) {
            return res.status(404).send({
                message: 'Invalid courses id'
            })
        }
        const chapters_id = courses.flatMap(course => course.chapters)
        const chapters = await Chapter.find({_id: {$in: chapters_id}})
        const materials_id = chapters.flatMap(chapter => chapter.materials)
        const materials = await Material.find({_id: {$in: materials_id}})

        const responses = courses.map(course => {
            const takescourse = takescourses.find(takescourse => takescourse.course_id === course._id.toString())
            const materials_done_id = takescourse.material_done
            const c_chapters = chapters.filter(chapter => course.chapters.includes(chapter._id.toString()))
            const c_materials_id = c_chapters.flatMap(chapter => chapter.materials)
            const c_materials = materials.filter(material => c_materials_id.includes(material._id.toString()))
            
            // const article_complete = c_materials.filter(material => material.type === 'article').length
            // const video_complete = c_materials.filter(material => material.type === 'video').length
            const article_complete = materials_done_id.filter(material_id => c_materials.find(material => material._id.toString() === material_id).type === 'article').length
            const video_complete = materials_done_id.filter(material_id => c_materials.find(material => material._id.toString() === material_id).type === 'video').length
            const assessment_complete = takescourse.assessment_done.length
            const total_article = c_materials.filter(material => material.type === 'article').length
            const total_video = c_materials.filter(material => material.type === 'video').length
            const total_assessment = course.total_assessment

            const progress = Math.round(((article_complete + video_complete + assessment_complete) / 
        (total_article + total_video + total_assessment)) * 100)
            return {
                course_id: course._id.toString(),
                thumbnail: course.thumbnail,
                title: course.title,
                topic: course.topic,
                creator: course.creator,
                progress: progress,
                article_complete: article_complete,
                video_complete: video_complete,
                assessment_complete: assessment_complete,
                total_article: total_article,
                total_video: total_video,
                total_assessment: total_assessment,
                //material_done: takescourse.material_done.length,
                //total_material: course.total_material,
                assessment_done: assessment_complete,
                total_assessment: total_assessment,
                //chapter_done: takescourse.chapter_done,
                //total_chapter: course.total_chapter,
                start_date: takescourse.start_date,
                finish_date: takescourse.finish_date
            }
        })

        return res.status(200).send({
            count: takescoursecount,
            data: responses
        })
    }catch(err) {
        console.log(err)
        return res.status(500).send({
            message: 'Internal server error',
            err: err
        })
    }
}

const getCoursesPublished = async (req, res, next) => {
    // Example of the response:
    // {
    // count: 5,
    // data: 
    // [{
    //     _id: '9gbdig9fgehe0dcj0',
    //     thumbnail: 'https://atariwiki.org/wiki/attach/6502%20Assembly%20Code/6502%20Assembly%20Code%20in%20Sublime%20Text.jpg',
    //     title: 'Dasar Assembly',
    //     topic: 'Informatika',
    //     creator: 'Randy Zakya Suchrady Alter.',
    //     description: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.',
    //     created_at: new Date(2022, 1, 2),
    //     last_updated_at: new Date(2022, 1, 4),
    //     taken_count: 451
    //     },...]
    // }
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params

    try {
        const courses = await Course.find({ contributor_id: user_id })
        .limit(limit * 1)
        .skip((page - 1) * limit)
        if (!courses) {
            return res.status(404).send({
                message: 'Invalid courses id'
            })
        }
        const coursecount = await Course.countDocuments({ contributor_id: user_id })
        const response = courses.map(course => {
            return {
                _id: course._id.toString(),
                thumbnail: course.thumbnail,
                title: course.title,
                topic: course.topic,
                creator: course.creator,
                description: course.description,
                created_at: course.created_at,
                last_updated_at: course.last_updated_at,
                taken_count: course.taken_count
            }
        })
        return res.status(200).send({
            count: coursecount,
            data: response
        })
    }catch(err) {
        return res.status(500).send({
            message: 'Internal server error',
            err: err
        })
    }
}


module.exports = { getProfile, updateProfile, changePassword , getCoursesProgress, getCoursesPublished}