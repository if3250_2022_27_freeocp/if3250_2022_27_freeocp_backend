const Question = require('../models/question')
const Assessment = require('../models/assessment')
const Material = require('../models/material')
const Chapter = require('../models/chapter')
const Course = require('../models/course')
const AssessmentCourse = require('../models/assessmentcourse')
const User = require('../models/users')


const deleteAll = async (req, res) => {
    const user_id = res.locals.id

    try {
        const user = await User.findById(user_id)
        //check if user is admin
        if (!user.is_admin) {
            res.status(401).json({
                message: 'You are not authorized to delete all data'
            })
        }
        // delete all questions, assessment, material, chapter, course
        await Question.deleteMany({})
        await Assessment.deleteMany({})
        await Material.deleteMany({})
        await Chapter.deleteMany({})
        await Course.deleteMany({})
        await AssessmentCourse.deleteMany({})
        return res.status(200).json({
            message: 'All data deleted'
        })
    }catch(err) {
        return res.status(500).json({
            message: "Internal Server Error",
            err: err,
        })
    }
}

module.exports = {
    deleteAll
}