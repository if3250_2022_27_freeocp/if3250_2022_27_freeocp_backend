const User = require('../models/users')
const Course = require('../models/course')

const str = require('@supercharge/strings')
const fs = require('fs')

const multer = require('multer')
const path = require('path')
const profpicStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname,'../img/profpic/'))
    },
    filename: function (req, file, cb) {
        cb(null, profilepicNameGenerator(req,file))
    }
})
const coursepicStorage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, path.join(__dirname,'../img/coursepic/'))
    },
    filename: function (req, file, cb) {
        cb(null, coursepicGenerator(req,file))
    }
})

const profpicUpload = multer({ storage: profpicStorage }).single('profpic')
const coursepicUpload = multer({ storage: coursepicStorage }).single('coursepic')

// https://stackoverflow.com/questions/39589022/node-js-multer-and-req-body-empty

const profilepicNameGenerator = (req,file) => {
    const randomStr = str.random(8)
    const profpic_name = `profile-${randomStr}-${Date.now()}${path.extname(file.originalname)}`
    req.body.profpic_url = req.protocol + '://' + req.get('host') + '/profpic/' + profpic_name
    return profpic_name
}

const coursepicGenerator = (req,file) => {
    const randomStr = str.random(8)
    const coursepic_name = `course-${randomStr}-${Date.now()}${path.extname(file.originalname)}`
    req.body.coursepic_url = req.protocol + '://' + req.get('host') + '/coursepic/' + coursepic_name
    return coursepic_name
}

const changeProfpicUrl = async (req,res,next) => {
    const user_id = res.locals.id
    const profpic_url = req.body.profpic_url

    const filepath = req.file.path

    const default_profpic = req.protocol + '://' + req.get('host') + '/profpic/default_profpic.png'

    try {
        const user = await User.findById(user_id)
        if (!user) {
            // delete the file
            return deleteProfilePic(res,filepath,coursepic_url,"Unauthorized user")
        }
        const old_profpic_url = user.profile_picture
        user.profile_picture = profpic_url
        if (old_profpic_url !== default_profpic && old_profpic_url !== null) {
            const old_profpic_filepath = path.join(__dirname,'../img/profpic/') + old_profpic_url.split('/').pop()
            fs.access(old_profpic_filepath, fs.constants.F_OK, (err) => {
                if (!err) {
                    fs.unlink(old_profpic_filepath, (err) => {
                        if (err) {
                            return res.status(500).json({
                                message: "Fail to change user profile picture",
                                old_profpic_url: old_profpic_url
                            })
                        }
                    })
                }
            })
        }
        await user.save()
        return res.status(200).json({
            message: 'Profile picture changed successfully',
            profpic_url: user.profile_picture
        })
    } catch (err) {
        return deleteProfilePic(res,filepath,profpic_url, "Fail to change user profile picture")
    }
}

const removeProfpic = async (req,res,next) => {
    const user_id = res.locals.id

    const default_profpic = req.protocol + '://' + req.get('host') + '/profpic/default_profpic.png'

    try {
        const user = await User.findById(user_id)
        if (!user) {
            return res.status(500).json({
                message: "Unauthorized user"
            })
        }
        const old_profpic_url = user.profile_picture
        user.profile_picture = default_profpic
        if (old_profpic_url !== default_profpic && old_profpic_url !== null) {
            const old_profpic_filepath = path.join(__dirname,'../img/profpic/') + old_profpic_url.split('/').pop()
            // check if old profpic exist in file system
            fs.access(old_profpic_filepath, fs.constants.F_OK, (err) => {
                if (!err) {
                    // only delete if the file exist
                    // if not then let it be
                    fs.unlink(old_profpic_filepath, (err) => {
                        if (err) {
                            return res.status(500).json({
                                message: "Fail to change course picture",
                                old_profpic_url: old_profpic_url
                            })
                        }
                    })
                }
            })
        }
        await user.save()
        return res.status(200).json({
            message: 'Profile picture changed successfully',
            profpic_url: user.profile_picture
        })
    } catch (err) {
        return res.status(500).json({
            message: "Fail to change user profile picture"
        })
    }
}

const changeCoursepicUrl = async (req,res,next) => {
    const course_id = req.body.course_id
    const coursepic_url = req.body.coursepic_url
    const user_id = res.locals.id

    const filepath = req.file.path

    const default_coursepic = req.protocol + '://' + req.get('host') + '/coursepic/default_coursepic.png'

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            // delete the file
            return deleteCoursePic(res,filepath,coursepic_url,"Fail to change course picture")
        }
        if (course.contributor_id !== user_id) {
            // delete the file
            return deleteCoursePic(res,filepath,coursepic_url,"Unauthorized user")
        }
        const old_thumbnail = course.thumbnail
        course.thumbnail = coursepic_url
        if (old_thumbnail !== default_coursepic && old_thumbnail !== null) {
            const old_thumbnail_filepath = path.join(__dirname,'../img/coursepic/') + old_thumbnail.split('/').pop()
            fs.access(old_thumbnail_filepath, fs.constants.F_OK, (err) => {
                if (!err) {
                    fs.unlink(old_thumbnail_filepath, (err) => {
                        if (err) {
                            return res.status(500).json({
                                message: "Fail to change course picture",
                                old_coursepic_url: old_thumbnail
                            })
                        }
                    })
                }
            })
        }
        await course.save()
        return res.status(200).json({
            message: 'Course picture changed successfully',
            coursepic_url: course.thumbnail
        })
    } catch (err) {
        return deleteCoursePic(res,filepath,coursepic_url,"Fail to change course picture")
    }
}

const removeCoursepic = async (req,res,next) => {
    const course_id = req.body.course_id
    const user_id = res.locals.id

    const default_coursepic = req.protocol + '://' + req.get('host') + '/coursepic/default_coursepic.png'

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(500).json({
                message: "Fail to change course picture, the course does not exist"
            })
        }
        if (course.contributor_id !== user_id) {
            return res.status(500).json({
                message: "Unauthorized user"
            })
        }
        const old_thumbnail = course.thumbnail
        course.thumbnail = default_coursepic

        if (old_thumbnail !== default_coursepic && old_thumbnail !== null) {
            const old_thumbnail_filepath = path.join(__dirname,'../img/coursepic/') + old_thumbnail.split('/').pop()
            fs.access(old_thumbnail_filepath, fs.constants.F_OK, (err) => {
                if (!err) {
                    fs.unlink(old_thumbnail_filepath, (err) => {
                        if (err) {
                            return res.status(500).json({
                                message: "Fail to change course picture",
                                old_coursepic_url: old_thumbnail
                            })
                        }
                    })
                }
            })
        }
        await course.save()
        return res.status(200).json({
            message: 'Course picture changed successfully',
            coursepic_url: course.thumbnail
        })
    } catch (err) {
        return res.status(500).json({
            message: "Fail to change course picture"
        })
    }
}

const deleteCoursePic = (res,filepath,coursepic_url,message) => {
    // delete the file
    fs.access(filepath, fs.constants.F_OK, (err) => {
        if (!err) {
            fs.unlink(filepath, (err) => {
                if (err) {
                    return res.status(500).json({
                        message: "Fail to change course picture but the file is exist in server",
                        coursepic_url: coursepic_url
                    })
                }
            })
        }
    })
    return res.status(500).json({
        message: message
    })
}

const deleteProfilePic = (res,filepath,profpic_url,message) => {
    fs.access(filepath, fs.constants.F_OK, (err) => {
        if (!err) {
            fs.unlink(filepath, (err) => {
                if (err) {
                    return res.status(500).json({
                        message: "Fail to change user profile picture but the file is exist in server",
                        profpic_url: profpic_url
                    })
                }
            })
        }
    })
    return res.status(500).json({
        message: message
    })
}

module.exports = {
    profpicUpload,
    coursepicUpload,
    changeProfpicUrl,
    changeCoursepicUrl,
    removeProfpic,
    removeCoursepic
}