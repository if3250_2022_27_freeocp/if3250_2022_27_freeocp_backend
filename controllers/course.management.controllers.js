const Course = require('../models/course')
const Material = require('../models/material')
const Chapter = require('../models/chapter')
const User = require('../models/users')
const AssessmentCourse = require('../models/assessmentcourse')
const Assessment = require('../models/assessment')

const db = require('../db/conn')
const mongoose = db.getMongoose()
const conn = mongoose.connection

// CREATE

// create material
const createMaterial = (req,res,next) => {
    const {title,type,link} = req.body
    // Check if the type is valid
    // Valid types : video, article, assessment
    if(type !== 'video' && type !== 'article' && type !== 'assessment'){
        return res.status(400).json({
            message : 'Invalid type'
        })
    }

    // Create the material
    const material = new Material({
        title : title,
        type : type,
        link : link,
    })

    // Save the material
    material.save((err,material) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }
        return res.status(201).json({
            message : 'Material created',
            material : material
        })
    })
}

// create chapters
const createChapter = (req,res,next) => {
    const {title,materials_id} = req.body
    // Create the chapter
    const chapter = new Chapter({
        title : title,
        materials : materials_id,
    })
    // Save the chapter
    chapter.save((err,chapter) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }
        return res.status(201).json({
            message : 'Chapter created',
            chapter : chapter
        })
    })
}
// create course
const createCourse = async (req,res,next) => {
    const user_id = res.locals.id
    // materials is an array of material object (to ease the total_assessment and total_material)
    const {title,topic,description,syllabus,chapters_id,materials} = req.body
    var thumbnail = req.body.thumbnail

    if (!thumbnail) {
        thumbnail = req.protocol + '://' + req.get('host') + '/coursepic/default_coursepic.png'
    }

    try {
        const user = await User.findById(user_id)
        if (!user) {
            return res.status(404).json({
                message: 'User not found'
            })
        }

        const assessments_id = materials.filter(material => material.type === 'assessment').map(material => material.link)
        const assessments = await Assessment.find({_id : {$in : assessments_id}})
        const acceptedAssessments = assessments.filter(assessment => assessment.status === 'accepted')
        const total_assessment = acceptedAssessments.length
        const total_material = materials.length - assessments.length

        const course = new Course({
            contributor_id : user_id,
            creator : user.full_name,
            title : title,
            topic : topic,
            thumbnail : thumbnail,
            description : description,
            created_at : new Date(),
            last_updated : new Date(),
            taken_count : 0,
            total_material : total_material,
            total_assessment : total_assessment,
            total_chapter : chapters_id.length,
            syllabus : syllabus,
            chapters : chapters_id,
        })

        const session = await conn.startSession()

        session.withTransaction(async () => {
            await course.save()
            const chapters = await Chapter.find({_id : {$in : chapters_id}})
            
            if (!chapters) {
                return res.status(400).json({
                    message: 'Chapter not found'
                })
            }

            const assessment_id = materials.filter(material => material.type === 'assessment').map(material => material.link)
            const assessmentCourses = []

            assessment_id.forEach(_id => {
                const assessmentcourse = new AssessmentCourse({
                    course_id : course._id,
                    course_title : course.title,
                    assessment_id : _id,
                })
                assessmentCourses.push(assessmentcourse)
            })

            await AssessmentCourse.insertMany(assessmentCourses)
        })

        // session.endSession()

        return res.status(201).json({
            message : 'Course created',
            course : course
        })
    }catch (err) {
        return res.status(500).json({
            message : 'Internal server error',
            err: err
        })
    }
}

// UPDATE

// update material
const updateMaterial = (req,res,next) => {
    const user_id = res.locals.id
    const {title,type,link} = req.body
    const {course_id,chapter_id,material_id} = req.params

    // Find the course
    Course.findById(course_id,(err,course) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }
        if(!course){
            return res.status(404).json({
                message : 'Course not found'
            })
        }
        // Check if the user is the contributor
        if(course.contributor_id !== user_id){
            return res.status(403).json({
                message : 'You are not the contributor'
            })
        }
        // Find the chapter
        Chapter.findById(chapter_id,(err,chapter) => {
            if(err){
                return res.status(500).json({
                    message : 'Internal server error',
                    err: err
                })
            }
            if(!chapter){
                return res.status(404).json({
                    message : 'Chapter not found'
                })
            }
            // Find the material
            Material.findById(material_id,(err,material) => {
                if(err){
                    return res.status(500).json({
                        message : 'Internal server error',
                        err: err
                    })
                }
                if(!material){
                    return res.status(404).json({
                        message : 'Material not found'
                    })
                }
                // Check if the type is valid
                // Valid types : video, article, assessment
                if (type) {
                    if(type !== 'video' && type !== 'article' && type !== 'assessment'){
                        return res.status(400).json({
                            message : 'Invalid type'
                        })
                    }
                }
                // Update the material
                material.title = title ? title : material.title
                material.type = type ? type : material.type
                material.link = link ? link : material.link
                // Save the material
                material.save((err,material) => {
                    if(err){
                        return res.status(500).json({
                            message : 'Internal server error',
                            err: err
                        })
                    }
                    return res.status(201).json({
                        message : 'Material updated',
                        material : material
                    })
                })
            })
        })
    })
}

// update chapter
const updateChapter = (req,res,next) => {
    const user_id = res.locals.id
    const {title} = req.body
    const {course_id,chapter_id} = req.params

    // Find the course
    Course.findById(course_id,(err,course) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }
        if(!course){
            return res.status(404).json({
                message : 'Course not found'
            })
        }
        // Check if the user is the contributor
        if(course.contributor_id !== user_id){
            return res.status(403).json({
                message : 'You are not the contributor'
            })
        }
        // Find the chapter
        Chapter.findById(chapter_id,(err,chapter) => {
            if(err){
                return res.status(500).json({
                    message : 'Internal server error',
                    err: err
                })
            }
            if(!chapter){
                return res.status(404).json({
                    message : 'Chapter not found'
                })
            }
            // Update the chapter
            chapter.title = title ? title : chapter.title
            // Save the chapter
            chapter.save((err,chapter) => {
                if(err){
                    return res.status(500).json({
                        message : 'Internal server error',
                        err: err
                    })
                }
                return res.status(201).json({
                    message : 'Chapter updated',
                    chapter : chapter
                })
            })
        })
    })
}

// edit link material
const editLinkMaterial = (req,res,next) => {
    const user_id = res.locals.id
    const {link} = req.body
    const {material_id} = req.params

    // Find material
    Material.findById(material_id,(err,material) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }
        if(!material){
            return res.status(404).json({
                message : 'Material not found'
            })
        }
        // Update the material
        material.link = link ? link : material.link
        // Save the material
        material.save((err,material) => {
            if(err){
                return res.status(500).json({
                    message : 'Internal server error',
                    err: err
                })
            }
            return res.status(201).json({
                message : 'Material updated',
                material : material
            })
        })
    })
}

const addAssessmentToChapter = async (req,res,next) => {
    const user_id = res.locals.id
    const {course_id, chapter_id} = req.params
    const {assessment_id} = req.body

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        if (course.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'You are not the contributor'
            })
        }

        const chapter = await Chapter.findById(chapter_id)
        if (!chapter) {
            return res.status(404).json({
                message: 'Chapter not found'
            })
        }

        const assessment = await Assessment.findById(assessment_id)
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        const newMaterial = new Material({
            title: assessment.title,
            type: 'assessment',
            link: assessment._id
        })

        chapter.materials.push(newMaterial._id.toString())
        if (assessment.status === 'accepted') {
            course.total_assessment++
        }

        const newAssessmentCourse = new AssessmentCourse({
            assessment_id: assessment._id,
            course_title: course.title,
            course_id: course._id
        })

        const session = await mongoose.startSession()

        session.withTransaction(async () => {
            await newAssessmentCourse.save()
            await newMaterial.save()
            await chapter.save()
            await course.save()
        })

        // session.endSession()

        return res.status(201).json({
            message: 'Assessment added to chapter',
            chapter: chapter
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err: err
        })
    }
}

// update course
const updateCourse = async (req,res,next) => {
    const user_id = res.locals.id
    const {title,topic,thumbnail,description,syllabus,chapters_id,materials} = req.body
    const {course_id} = req.params
    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }
        if (course.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'You are not the contributor'
            })
        }

        course.title = title ? title : course.title
        course.topic = topic ? topic : course.topic
        course.thumbnail = thumbnail ? thumbnail : course.thumbnail
        course.description = description ? description : course.description
        course.syllabus = syllabus ? syllabus : course.syllabus
        course.chapters = chapters_id ? chapters_id : course.chapters
        // course.total_assessment = materials ? materials.filter(material => material.type === 'assessment').length : course.total_assessment
        course.total_chapter = chapters_id ? chapters_id.length : course.total_chapter
        course.last_updated = new Date()

        if (materials) {
            const assessments_id = materials.filter(material => material.type === 'assessment').map(material => material.link)
            const assessments = await Assessment.find({_id: {$in: assessments_id}})
            const acceptedAssessments = assessments.filter(assessment => assessment.status === 'accepted')

            course.total_assessment = acceptedAssessments.length
        }
        course.total_material = materials ? materials.length - course.total_assessment : course.total_material

        course.save()

        return res.status(201).json({
            message : 'Course updated',
            course : course
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err: err
        })
    }
}

// DELETE

// delete materials with ids
const deleteMaterials = (req,res,next) => {
    const {materials_id} = req.body

    // Delete the materials
    Material.deleteMany({_id : {$in : materials_id}},(err,materials) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }
    })
}

// delete chapters with ids
const deleteChapters = (req,res,next) => {
    const {chapters_id} = req.body

    // Delete the chapters
    Chapter.deleteMany({_id : {$in : chapters_id}},(err,chapters) => {
        if(err){
            return res.status(500).json({
                message : 'Internal server error',
                err: err
            })
        }

        // Delete the material
        const materials_id = chapters.map(chapter => chapter.materials_id)
        // call deleteMaterials
        deleteMaterials({body : {materials_id}},res,next)
    })
}

// delete one material
const deleteMaterial = async (req,res,next) => {
    const user_id = res.locals.id
    const {course_id,chapter_id,material_id} = req.params

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        if (course.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'You are not the contributor'
            })
        }

        const chapter = await Chapter.findById(chapter_id)
        if (!chapter) {
            return res.status(404).json({
                message: 'Chapter not found'
            })
        }

        const material = await Material.findById(material_id)
        if (!material) {
            return res.status(404).json({
                message: 'Material not found'
            })
        }

        chapter.materials.splice(chapter.materials.indexOf(material_id),1)
        // decrement the total_material is not type assessment
        if(material.type !== 'assessment'){
            course.total_material--
        }else {
            const assessment_id = material.link
            const assessment = await Assessment.findById(assessment_id)
            if (assessment.status === 'accepted') {
                course.total_assessment--
            }
        }

        const session = await mongoose.startSession()

        session.withTransaction(async () => {
            await material.remove()
            await chapter.save()
            await course.save()
        })

        // session.endSession()

        return res.status(200).json({
            message: 'Material deleted',
            material: material
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err: err
        })
    }
}

// delete one chapter and calling delete materials
const deleteChapter = async (req,res,next) => {
    const user_id = res.locals.id
    const {course_id,chapter_id} = req.params

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        if (course.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'You are not the contributor'
            })
        }

        const chapter = await Chapter.findById(chapter_id)
        if (!chapter) {
            return res.status(404).json({
                message: 'Chapter not found'
            })
        }

        const materials_id = chapter.materials_id
        course.total_chapter--

        const session = await mongoose.startSession()

        session.withTransaction(async () => {
            await Material.deleteMany({_id : {$in : materials_id}})
            await chapter.remove()
            await course.save()
        })

        // session.endSession()

        return res.status(200).json({
            message: 'Chapter deleted',
            chapter: chapter
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err: err
        })
    }
}

// delete one course and calling delete chapters in this course
const deleteCourse = async (req,res,next) => {
    const user_id = res.locals.id
    const {course_id} = req.params

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        if (course.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'You are not the contributor'
            })
        }

        const chapters_id = course.chapters
        const chapters = await Chapter.find({_id : {$in : chapters_id}})
        const materials_id = chapters.flatMap(chapter => chapter.materials)
        const materials = await Material.find({_id : {$in : materials_id}})

        const session = await mongoose.startSession()

        session.withTransaction(async () => {
            await Material.deleteMany({_id : {$in : materials_id}})
            await Chapter.deleteMany({_id : {$in : chapters_id}})
            await course.remove()
        })

        // session.endSession()

        return res.status(200).json({
            message: 'Course deleted',
            course: course
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err: err
        })
    }
}

module.exports = {
    createCourse,
    updateCourse,
    deleteCourse,
    createChapter,
    updateChapter,
    deleteChapter,
    createMaterial,
    updateMaterial,
    deleteMaterial,
    editLinkMaterial,
    addAssessmentToChapter,
}