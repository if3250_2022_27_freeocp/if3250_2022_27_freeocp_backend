const Course = require('../models/course')
const TakesCourse = require('../models/takescourse')
const Material = require('../models/material')
const Chapter = require('../models/chapter')


/*
    example response
    {
        course_id: 21398432bh42394811,
        user_id: 123456789,
        progress: 50,
        article_complete: 12,
        video_complete: 2,
        assessment_complete: 3,
        total_article: 24,
        total_video: 4,
        total_assessment: 6,
        start_date: (Date Data),
        finish_date: (Date Data) | null,
    }
*/

const getProgress = async (req,res,next) => {
    const user_id = res.locals.id
    const course_id = req.params.course_id


    try {

        const takescourse = await TakesCourse.findOne({user_id: user_id, course_id: course_id})

        if (!takescourse) {
            return res.status(404).json({
                message: 'User has not taken this course yet'
            })
        }

        const course = await Course.findById(course_id)
        const chapters_id = course.chapters
        const chapters = await Chapter.find({_id: {$in: chapters_id}})
        const materials_id = chapters.flatMap(chapter => chapter.materials)
        const materials = await Material.find({_id: {$in: materials_id}})

        const materials_done_id = takescourse.material_done // non assessment material

        const article_complete = materials_done_id.filter(material_id => materials.find(material => material._id == material_id).type == 'article').length
        const video_complete = materials_done_id.filter(material_id => materials.find(material => material._id == material_id).type == 'video').length
        const assessment_complete = takescourse.assessment_done.length
        const total_article = materials.filter(material => material.type === 'article').length
        const total_video = materials.filter(material => material.type === 'video').length
        const total_assessment = course.total_assessment
        const start_date = takescourse.start_date
        const finish_date = takescourse.finish_date
        const progress = Math.round(((article_complete + video_complete + assessment_complete) / 
        (total_article + total_video + total_assessment)) * 100)

        const response = {
            course_id: course_id,
            user_id: user_id,
            progress: progress,
            article_complete: article_complete,
            video_complete: video_complete,
            assessment_complete: assessment_complete,
            total_article: total_article,
            total_video: total_video,
            total_assessment: total_assessment,
            start_date: start_date,
            finish_date: finish_date,
        }

        return res.status(200).json(response)
    }catch(err) {
        res.status(500).json({
            message: 'Internal Server Error',
            err: err
        })
    }
}


module.exports = {
    getProgress
}