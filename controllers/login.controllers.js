const User = require('../models/users')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
const bcrypt = require('bcrypt')
dotenv.config()

const login = (req, res, next) => {
	const { email, password } = req.body

	User.findOne({ 'email': email }, function (err, result) {
		if (err) {
			return next(err)
		}
		if (result != null) {
			const isPasswordMatch = bcrypt.compareSync(password, result.password)
			if (isPasswordMatch) {
				const id = result["_id"]
				const email = result["email"]
				const profile_picture = result["profile_picture"]
				const token = jwt.sign({"id": id}, process.env.JWT_SECRET, {
					expiresIn: 7200,
				})
				return res.status(200).json({
					auth: true,
					token: token,
					email: email,
					profile_picture: profile_picture
				})
			}else {
				return res.status(401).json({
					auth: false,
					message: "Email and password don't match"
				})
			}
		}
		else {
			return res.status(401).json({
				auth: false,
				message: "Email and password don't match"
			})
		}
	});
}

module.exports = {
  login
}
