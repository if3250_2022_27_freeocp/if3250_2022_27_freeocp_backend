const Course = require('../models/course')
const TakesCourse = require('../models/takescourse')

const db = require('../db/conn')
const mongoose = db.getMongoose()
const conn = mongoose.connection


const enrollCourse = async (req, res, next) => {
    const { course_id } = req.params
    const user_id = res.locals.id

    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        const takescourse = await TakesCourse.findOne({
            user_id: user_id,
            course_id: course_id
        })

        if (takescourse) {
            return res.status(400).json({
                message: 'User already enrolled in this course'
            })
        }

        const newTakesCourse = new TakesCourse({
            user_id: user_id,
            course_id: course_id,
            material_done: [],
            assessment_done: [],
            assessment_score: [],
            chapter_done: 0,
            start_date: new Date(),
            finish_date: null,
            progress: 0
        })

        const session = await conn.startSession()

        session.withTransaction(async () => {
            await newTakesCourse.save()
            // increment taken_count in course
            await Course.findByIdAndUpdate(course_id, {
                $inc: { taken_count: 1 }
            }, { session })
        })

        // session.endSession()

        return res.status(200).json({
            message: 'User enrolled in course successfully',
            takesCourse: newTakesCourse
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal Server Error',
            err: err
        })
    }
}

module.exports = {enrollCourse}