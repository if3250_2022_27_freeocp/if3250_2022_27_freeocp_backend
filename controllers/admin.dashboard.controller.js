const User = require('../models/users')
const CuratorRegistration = require('../models/curatorregistration')

const db = require('../db/conn')
const mongoose = db.getMongoose()
const conn = mongoose.connection

// REGISTRATION STATUSES : pending, approved, rejected
const curatorRegistrationResponse = (curatorRegistrations, usersInfo) => {
    const response = []
    for (let i = 0; i < curatorRegistrations.length; i++) {
        const curatorRegistration = curatorRegistrations[i]
        const userInfo = usersInfo.find(user => user._id.toString() === curatorRegistration.user_id.toString())
        response.push({
            curator_registration_id: curatorRegistration._id,
            user_id: curatorRegistration.user_id,
            status: curatorRegistration.status,
            submission_date: curatorRegistration.submission_date,
            user_info: {
                full_name: userInfo.full_name,
                email: userInfo.email,
                institution: userInfo.institution,
                profile_picture: userInfo.profile_picture,
                description: userInfo.description
            }
        })
    }

    return response
}

const getCuratorRegistrations = async (page, limit, condition, message, res) => {

    try {
        const curatorRegistrations = await CuratorRegistration.find(condition)
        .limit(limit*1)
        .skip((page-1)*limit)

        const curatorRegistrationsCount = await CuratorRegistration.countDocuments(condition)

        const allCuratorUserId = curatorRegistrations.map(curatorRegistration => curatorRegistration.user_id)
        const allUserInfo = await User.find({_id: {$in: allCuratorUserId}})

        const responseData = curatorRegistrationResponse(curatorRegistrations, allUserInfo)

        return res.status(200).json({
            count: curatorRegistrationsCount,
            message : message,
            curator_registrations : responseData
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}

// GET LIST OF CURATOR REGISTRATIONS
const getAllCuratorRegistrations = async (req, res, next) => {
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    const statuses = ['pending', 'approved', 'rejected']
    const condition = {status: {$in: statuses}}
    try {
        // check if user is admin
        const user = await User.findById(user_id)
        if (!user.is_admin) {
            return res.status(403).json({
                message: 'You are not authorized to access this resource.'
            })
        }

        await getCuratorRegistrations(page, limit, condition, 'All curator registrations', res)
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}

// GET LIST OF CURATOR REGISTRATIONS PENDING
const getPendingCuratorRegistrations = async (req, res, next) => {
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    const condition = {status: 'pending'}

    try {
        // check if user is admin
        const user = await User.findById(user_id)
        if (!user.is_admin) {
            return res.status(403).json({
                message: 'You are not authorized to access this resource.'
            })
        }

        await getCuratorRegistrations(page, limit, condition, 'All pending curator registrations', res)
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}

// GET LIST OF CURATOR REGISTRATIONS APPROVED
const getApprovedCuratorRegistrations = async (req, res, next) => {
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    const condition = {status: 'approved'}

    try {
        // check if user is admin
        const user = await User.findById(user_id)
        if (!user.is_admin) {
            return res.status(403).json({
                message: 'You are not authorized to access this resource.'
            })
        }
        await getCuratorRegistrations(page, limit, condition, 'All approved curator registrations', res)
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}

// GET LIST OF CURATOR REGISTRATIONS REJECTED
const getRejectedCuratorRegistrations = async (req, res, next) => {
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    const condition = {status: 'rejected'}

    try {
        // check if user is admin
        const user = await User.findById(user_id)
        if (!user.is_admin) {
            return res.status(403).json({
                message: 'You are not authorized to access this resource.'
            })
        }
        await getCuratorRegistrations(page, limit, condition, 'All rejected curator registrations', res)
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}

// GET LIST OF CURATOR REGISTRATIONS NOT PENDING
const getNotPendingCuratorRegistrations = async (req, res, next) => {
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    const statuses = ['approved', 'rejected']
    const condition = {status: {$in: statuses}}
    // check if user is admin
    const user = await User.findById(user_id)
    if (!user.is_admin) {
        return res.status(403).json({
            message: 'You are not authorized to access this resource.'
        })
    }

    await getCuratorRegistrations(page, limit, condition, 'All not pending curator registrations', res)
}

// CHANGE STATUS FROM PENDING TO APPROVED
const approveCuratorRegistration = async (req, res, next) => {
    const user_id = res.locals.id
    const {curator_registration_id} = req.params
    // check if user is admin
    const user = await User.findById(user_id)
    if (!user.is_admin) {
        return res.status(403).json({
            message: 'You are not authorized to access this resource.'
        })
    }

    try {
        const curatorRegistration = await CuratorRegistration.findById(curator_registration_id)
        if (!curatorRegistration) {
            return res.status(404).json({
                message: 'Curator registration not found.'
            })
        }

        if (curatorRegistration.status === 'approved') {
            return res.status(400).json({
                message: 'Curator registration is already approved.'
            })
        }

        if (curatorRegistration.status === 'rejected') {
            return res.status(400).json({
                message: 'Curator registration is already rejected.'
            })
        }

        const session = await conn.startSession()

        await session.withTransaction(async () => {
            await CuratorRegistration.findByIdAndUpdate(curator_registration_id, {status: 'approved'})
            await User.findByIdAndUpdate(curatorRegistration.user_id, {is_curator: true})
        })

        // session.endSession()

        return res.status(200).json({
            message: 'Curator registration approved.'
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}

// CHANGE STATUS FROM PENDING TO REJECTED
const rejectCuratorRegistration = async (req, res, next) => {
    const user_id = res.locals.id
    const {curator_registration_id} = req.params
    // check if user is admin
    const user = await User.findById(user_id)
    if (!user.is_admin) {
        return res.status(403).json({
            message: 'You are not authorized to access this resource.'
        })
    }

    try {
        const curatorRegistration = await CuratorRegistration.findById(curator_registration_id)
        if (!curatorRegistration) {
            return res.status(404).json({
                message: 'Curator registration not found.'
            })
        }

        if (curatorRegistration.status === 'approved') {
            return res.status(400).json({
                message: 'Curator registration is already approved.'
            })
        }

        if (curatorRegistration.status === 'rejected') {
            return res.status(400).json({
                message: 'Curator registration is already rejected.'
            })
        }

        await CuratorRegistration.findByIdAndUpdate(curator_registration_id, {status: 'rejected'})

        return res.status(200).json({
            message: 'Curator registration rejected.'
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error.'
        })
    }
}


module.exports = {
    getAllCuratorRegistrations,
    getPendingCuratorRegistrations,
    getNotPendingCuratorRegistrations,
    getApprovedCuratorRegistrations,
    getRejectedCuratorRegistrations,
    approveCuratorRegistration,
    rejectCuratorRegistration
}