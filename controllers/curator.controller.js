const User = require('../models/users')
const Assessment = require('../models/assessment')
const AssessmentCourse = require('../models/assessmentcourse')
const CuratorRegistration = require('../models/curatorregistration')
const Course = require('../models/course')
const Feedback = require('../models/feedback')

const db = require('../db/conn')
const mongoose = db.getMongoose()
const conn = mongoose.connection


// REGISTER AS CURATOR

const becomeACurator = (req, res) => {
    const user_id = res.locals.id
    const agree_statement = req.body.agree_statement
    
    // Find user id
    User.findById(user_id, (err, user) => {
        if (err) {
            return res.status(500).json({
                message: 'Internal server error',
                err: err
            })
        }

        if (!user) {
            return res.status(404).json({
                message: 'User not found'
            })
        }

        if (user.is_curator) {
            return res.status(400).json({
                message: 'User is already a curator'
            })
        }

        if (agree_statement !== "Ya, saya setuju") {
            return res.status(400).json({
                message: 'User did not agree to become a curator'
            })
        }

        const newCuratorRegistration = new CuratorRegistration({
            user_id: user_id,
            status: 'pending',
            submission_date: new Date()
        })

        newCuratorRegistration.save((err, curatorRegistration) => {
            if (err) {
                return res.status(500).json({
                    message: 'Internal server error',
                    err: err
                })
            }

            return res.status(200).json({
                message: 'Registration as curator success, please wait for approval'
            })
        })
    })
}

const reviewAssessment = (req, res, next) => {
    const user_id = res.locals.id
    const assessment_id = req.params.assessment_id

    Assessment.findById(assessment_id, (err, assessment) => {
        if (err) {
            return res.status(500).json({
                message: 'Internal server error',
                err: err
            })
        }

        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        if (assessment.contributor_id === user_id) {
            return res.status(400).json({
                message: 'User is the contributor, cannot review their own assessment'
            })
        }

        if (assessment.curator_id !== null && assessment.curator_id !== user_id) {
            return res.status(400).json({
                message: 'Assessment is already curated by someone else'
            })
        }

        if (assessment.status !== 'pending') {
            return res.status(400).json({
                message: 'Assessment is already curated or in the process of curation'
            })
        }

        // Update assessment
        assessment.status = 'in review'
        assessment.curator_id = user_id
        assessment.save((err, assessment) => {
            if (err) {
                return res.status(500).json({
                    message: 'Internal server error',
                    err: err
                })
            }

            return res.status(200).json({
                message: 'Assessment status changed to review',
                assessment: assessment
            })
        })
    })
}

const acceptAssessment = async (req, res, next) => {
    const user_id = res.locals.id
    const assessment_id = req.params.assessment_id

    try {
        const assessment = await Assessment.findById(assessment_id)
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        if (assessment.contributor_id === user_id) {
            return res.status(400).json({
                message: 'User is the contributor, cannot review their own assessment'
            })
        }

        if (assessment.curator_id !== user_id && assessment.curator_id !== null) {
            return res.status(400).json({
                message: 'Assessment is already curated by someone else'
            })
        }

        if (assessment.status !== 'in review') {
            return res.status(400).json({
                message: 'Assessment is not in review'
            })
        }

        const assessmentCourse = await AssessmentCourse.findOne({
            assessment_id: assessment_id
        })

        if (!assessmentCourse) {
            return res.status(404).json({
                message: 'Assessment is not associated with any course'
            })
        }

        const course = await Course.findById(assessmentCourse.course_id)

        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        assessment.status = 'accepted'
        course.total_assessment ++

        const session = await conn.startSession()

        session.withTransaction(async () => {
            await assessment.save()
            await course.save()
        })

        return res.status(200).json({
            message: 'Assessment status changed to accepted',
            assessment: assessment
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }

    // Assessment.findById(assessment_id, (err, assessment) => {
    //     if (err) {
    //         return res.status(500).json({
    //             message: 'Internal server error',
    //             err: err
    //         })
    //     }

    //     if (!assessment) {
    //         return res.status(404).json({
    //             message: 'Assessment not found'
    //         })
    //     }

    //     if (assessment.contributor_id === user_id) {
    //         return res.status(400).json({
    //             message: 'User is the contributor, cannot review their own assessment'
    //         })
    //     }

    //     if (assessment.curator_id != user_id && assessment.curator_id != null) {
    //         return res.status(400).json({
    //             message: 'Assessment is already curated by someone else'
    //         })
    //     }

    //     if (assessment.status != 'in review') {
    //         return res.status(400).json({
    //             message: 'Assessment is not in review'
    //         })
    //     }

    //     // Update assessment
    //     assessment.status = 'accepted'
    //     assessment.save((err, assessment) => {
    //         if (err) {
    //             return res.status(500).json({
    //                 message: 'Internal server error',
    //                 err: err
    //             })
    //         }

    //         return res.status(200).json({
    //             message: 'Assessment status changed to accepted',
    //             assessment: assessment
    //         })
    //     })
    // })
}

const declineAssessment = async (req, res, next) => {
    const user_id = res.locals.id
    const assessment_id = req.params.assessment_id
    const feedback = req.body.feedback

    // find assessment
    const assessment = await Assessment.findById(assessment_id)

    if (!assessment) {
        return res.status(404).json({
            message: 'Assessment not found'
        })
    }

    if (assessment.contributor_id === user_id) {
        return res.status(400).json({
            message: 'User is the contributor, cannot review their own assessment'
        })
    }

    if (assessment.curator_id !== user_id && assessment.curator_id !== null) {
        return res.status(400).json({
            message: 'Assessment is already curated by someone else'
        })
    }

    if (assessment.status !== 'in review') {
        return res.status(400).json({
            message: 'Assessment is not in review'
        })
    }

    // Update assessment
    assessment.status = 'declined'

    // create feedback
    const newFeedback = new Feedback({
        assessment_id: assessment_id,
        contributor_id: assessment.contributor_id,
        curator_id: user_id,
        feedback: feedback
    })

    // create transaction to update assessment and create feedback
    try {
        const session = await conn.startSession()

        await session.withTransaction(async () => {
            // update assessment
            assessment.save()
            if (feedback) {
                await newFeedback.save()
            }
        })

        return res.status(200).json({
            message: 'Assessment status changed to declined',
            assessment: assessment,
            feedback: feedback ? newFeedback : null
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }
}

const getAssessmentInReviewByCuratorId = async (req, res, next) => {
    const user_id = res.locals.id

    const {page = 1, limit = 8} = req.params
    const statuses = ['in review']
    const condition = {curator_id: user_id, status: {$in: statuses}, contributor_id: {$ne: user_id}}
    const message = 'Reviewed assessments'
    return await getCurationAssessments(page, limit, condition, message, res)
}

const getAssessmentAcceptedByCuratorId = async (req, res, next) => {
    const user_id = res.locals.id

    const {page = 1, limit = 8} = req.params
    const statuses = ['accepted']
    const condition = {curator_id: user_id, status: {$in: statuses}, contributor_id: {$ne: user_id}}
    const message = 'Accepted assessments'
    return await getCurationAssessments(page, limit, condition, message, res)
}

const getAssessmentDeclinedByCuratorId = async (req, res, next) => {
    const user_id = res.locals.id

    const {page = 1, limit = 8} = req.params
    const statuses = ['declined']
    const condition = {curator_id: user_id, status: {$in: statuses}, contributor_id: {$ne: user_id}}
    const message = 'Declined assessments'
    return await getCurationAssessments(page, limit, condition, message, res)
}

/*
Example response:
[
    {
        "id": "5c9f8f8f8f8f8f8f8f8f8f8",
        "assessment_title": "Assessment 1",
        "course_title": "Course 1",
        "created_date": "2020-01-01",
    },
    {
        "id": "5c9f8f8f8f8f8f8f8f8f8f9",
        "assessment_title": "Assessment 2",
        "course_title": "Course 2",
        "created_date": "2020-01-01",
    },...
]
*/
const getAllPendingAssessments = async (req, res, next) => {
    const user_id = res.locals.id
    const {page = 1, limit = 8} = req.params
    const statuses = ['pending']
    const condition = {status: {$in: statuses}, contributor_id: {$ne: user_id}}
    const message = 'Pending assessments'
    return await getCurationAssessments(page, limit, condition, message, res)
}

const getCurationAssessments = async (page, limit, condition, message, res) => {
    try {
        const assessmentByStatus = await Assessment.find(condition)
        .limit(limit * 1)
        .skip((page - 1) * limit)

        const assessments_id = assessmentByStatus.map(assessment => assessment._id.toString())
        const assessmentCourses = await AssessmentCourse.find({ assessment_id: { $in: assessments_id } })
        const assessmentCounts = await Assessment.countDocuments(condition)

        const allResponse = assessmentResponses(assessmentByStatus, assessmentCourses)

        return res.status(200).json({
            count: assessmentCounts,
            message : message,
            assessments : allResponse
        })
    }catch(err){
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }
}

const assessmentResponses = (assessments,assessmentCourses) => {
    const allResponse = []
    for (let i = 0; i < assessments.length; i++) {
        const assessment = assessments[i]
        const assessmentId = assessment._id.toString()
        const assessmentCoursesFiltered = assessmentCourses.filter(assessmentCourse => assessmentCourse.assessment_id == assessmentId)
        // const course_title = assessmentCoursesFiltered[0] ? assessmentCoursesFiltered[0].course_title : null
        const course_title = assessmentCoursesFiltered[0].course_title
        const response = {
            id: assessmentId,
            assessment_title: assessment.title,
            course_title: course_title,
            created_date: assessment.created_date,
            status : assessment.status
        }
        allResponse.push(response)
    }

    return allResponse
}

const getAssessmentByCuratorId = async (req,res,next) => {
    const user_id = res.locals.id

    const {page = 1, limit = 8} = req.params
    const statuses = ['in review','accepted','declined']
    const condition = {curator_id: user_id, status: {$in: statuses}, contributor_id: {$ne: user_id}}
    const message = `All assessments curated by user with id ${user_id}`
    return await getCurationAssessments(page, limit, condition, message, res)
}

// pending + not pending
const getPendingAndAll = async (req,res,next) => {
    const user_id = res.locals.id

    const {page = 1, limit = 8} = req.params
    const statuses = ['in review','accepted','declined']
    const condition = {$or: [{curator_id: user_id, status: {$in: statuses}},{status: 'pending'}], contributor_id: {$ne: user_id}}
    const message = `All assessments`
    return await getCurationAssessments(page, limit, condition, message, res)
}

module.exports = {
    becomeACurator,
    reviewAssessment,
    acceptAssessment,
    declineAssessment,
    getAssessmentInReviewByCuratorId,
    getAssessmentAcceptedByCuratorId,
    getAssessmentDeclinedByCuratorId,
    getAllPendingAssessments,
    getAssessmentByCuratorId,
    getPendingAndAll,
}