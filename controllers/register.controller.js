const User = require('../models/users')
const bcrypt = require('bcrypt')

const register = (req, res, next) => {
    // To simplify the variables, I'm using the same name as the model
    const { email, full_name, password, password_confirmation } = req.body;

    // Check if email valid
    const isEmailValid = email.match(
        /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    );
    if (!isEmailValid) {
        res.status(400).send({
            message: 'Email is not valid'
        })
        return
    }

    // Check if full name provided
    if (full_name.length == 0) {
        res.status(400).send({
            message: 'Full name is not provided'
        })
        return
    }

    // Check if the password length is more than 8 characters
    if (password.length < 8) {
        res.status(400).send({
            message: 'Password must be at least 8 characters'
        })
        return
    }

    // Check if password only consist of alphabets, underscore, numbers, hyphen, and period
    if (!password.match(/^[a-zA-Z0-9_\-\.]+$/)) {
        res.status(400).send({
            message: 'Password must only consist of alphabets, underscore, numbers, hyphen, and period'
        })
        return
    }

    // Check if password consist of at least one number, one uppercase letter, and one lowercase letter
    if (!password.match(/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{8,}$/)) {
        res.status(400).send({
            message: 'Password must consist of at least one number, one uppercase letter, and one lowercase letter'
        })
        return
    }

    // Check if password and password confirmation are the same
    if (password !== password_confirmation) {
        res.status(400).send({
            message: 'Password and password confirmation must be the same'
        })
        return
    }

    // Check if the email is already taken
    User.findOne({
        email: email
    }, (err, result) => {
        if (err) {
            res.status(500).send({
                message: err
            })
            return
        }

        if (result) {
            res.status(409).send({
                message: 'Email is already taken'
            })
            return
        }

        // Hash the password
        const salt = bcrypt.genSaltSync()
        const hashedPassword = bcrypt.hashSync(password, salt)

        // api:port/profpic/default_profpic.png

        // If all the checks are passed, create a new user
        const newUser = new User({
            email: email,
            password: hashedPassword,
            full_name: full_name,
            date_of_birth: null,
            is_curator: false,
            profile_picture: req.protocol + '://' + req.get('host') + '/profpic/default_profpic.png',
            institution: null,
		    description: null,
            is_admin: false
        })

        newUser.save()
            .then()
            .catch((err) => console.log(err))
        
        // Send a success response
        res.status(201).send({
            message: 'Successfully created a new user'
        });
    })

};

module.exports = {register};