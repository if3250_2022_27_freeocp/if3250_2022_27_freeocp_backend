const Assessment = require('../models/assessment')
const Question = require('../models/question')
const TakesAssessment = require('../models/takesassessment')
const Feedback = require('../models/feedback')

const db = require('../db/conn')
const mongoose = db.getMongoose()
const conn = mongoose.connection

// CREATE

const createQuestion = (req, res, next) => {
    const {type,description,options,solution} = req.body

    // check if type is valid
    // Valid types : 'multiple-choice', 'multiple-answer'
    if (type !== 'multiple-choice' && type !== 'multiple-answer') {
        return res.status(400).json({
            message: 'Invalid type'
        })
    }

    // if type is multiple-choice or multiple-answer, check if options is an array
    if (!Array.isArray(options)) {
        return res.status(400).json({
            message: 'Invalid options'
        })
    }
    
    // if type is multiple answer, check if solution is an array
    if (type === 'multiple-answer') {
        if (!Array.isArray(solution)) {
            return res.status(400).json({
                message: 'Invalid solution'
            })
        }
    }

    // check if type is multiple-choice, solution is object
    if (type === 'multiple-choice') {
        if (typeof solution !== 'object') {
            return res.status(400).json({
                message: 'Invalid solution'
            })
        }
    }

    const newQuestion = new Question({
        type: type,
        description: description,
        options: options,
        solution: solution,
        created_date: new Date(),
        last_updated: new Date(),
    })

    // save
    newQuestion.save((err, question) => {
        if (err) {
            return res.status(500).json({
                message: 'Internal server error',
                err: err
            })
        }
        // send back the question
        res.status(201).json({
            message: 'Question created',
            question: question
        })
    })


}

const createAssessment = (req, res, next) => {
    const user_id = res.locals.id
    const {title,questions_id} = req.body

    const newAssessment = new Assessment({
        contributor_id: user_id,
        title: title,
        questions: questions_id,
        curator_id: null,
        status: 'pending',
        created_date: new Date(),
        last_updated: new Date(),
    })

    // save
    newAssessment.save((err, assessment) => {
        if (err) {
            return res.status(500).json({
                message: 'Internal server error',
                err: err
            })
        }
        // send back the assessment
        res.status(201).json({
            message: 'Assessment created',
            assessment: assessment
        })
    })
}

// READ

// Example response
// const assessmentData = {
//     title: 'Latihan Matematika',
//     questions: [
//       {
//         type: 'multiple_choice',
//         description: 'Berapa hasil dari 3 x 5?',
//         options: [
//           '15',
//           '10',
//           '5'
//         ],
//         solution: {
//           index: 0,
//           text: '15'
//         }
//       },
//       {
//         type: 'multiple_answer',
//         description: 'Operasi apa saja yang menghasilkan 15?',
//         options: [
//           '3 x 5',
//           '2 x 5',
//           '12 + 3'
//         ],
//         solutions: [
//           {
//             index: 0,
//             text: '3 x 5'
//           },
//           {
//             index: 2,
//             text: '12 + 3'
//           },
//         ]
//       },
//       {
//         type: 'short_text',
//         description: 'Berapa 2 x 4?',
//         solution: '8'
//       },
//     ]
//   }

const getAssessment = async (req, res, next) => {
    const assessment_id = req.params.id
    const user_id = res.locals.id

    try {
        const assessment = await Assessment.findById(assessment_id)
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }
        const questions = await Question.find({_id: {$in: assessment.questions}})
        if (!questions) {
            return res.status(404).json({
                message: 'Question not found'
            })
        }

        const newResponse = {
            title: assessment.title,
            status: assessment.status,
            questions: [],
        }

        questions.forEach(question => {
            if (question.type === 'multiple-choice') {
                newResponse.questions.push({
                    _id : question._id,
                    type: question.type,
                    description: question.description,
                    options: question.options,
                    solution: question.solution,
                })
            }else if (question.type === 'multiple-answer') {
                newResponse.questions.push({
                    _id : question._id,
                    type: question.type,
                    description: question.description,
                    options: question.options,
                    solutions: question.solution,
                })
            }
        })

        const takesassessment = await TakesAssessment.findOne({assessment_id: assessment_id, user_id: user_id})
        newResponse.takesassessment = takesassessment

        res.status(200).json(newResponse)
    }catch(err){
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }
}

// UPDATE

const updateQuestion = (req, res, next) => {
    const user_id = res.locals.id
    const {type,description,options,solution} = req.body
    const {question_id,assessment_id} = req.params

    Assessment.findById(assessment_id, (err, assessment) => {
        if (err) {
            return res.status(500).json({
                message: 'Internal server error',
                err: err
            })
        }
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }
        // check if question_id is in assessment.questions
        if (!assessment.questions.includes(question_id)) {
            return res.status(404).json({
                message: 'Question not found'
            })
        }
        if (assessment.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'Unauthorized'
            })
        }

        Question.findById(question_id, (err, question) => {
            question.type = type ? type : question.type
            question.description = description ? description : question.description
            question.options = options ? options : question.options
            question.solution = solution ? solution : question.solution
            question.last_updated = new Date()
    
            // check type is valid
            if (question.type !== 'multiple-choice' && question.type !== 'multiple-answer') {
                return res.status(400).json({
                    message: 'Invalid type'
                })
            }
    
            // if type is multiple-choice or multiple-answer, check if options is an array
            if (!Array.isArray(question.options)) {
                return res.status(400).json({
                    message: 'Invalid options'
                })
            }
    
            // if type is multiple-answer, check if solution is an array
            if (question.type === 'multiple-answer') {
                if (!Array.isArray(question.solution)) {
                    return res.status(400).json({
                        message: 'Invalid solution'
                    })
                }
            }
    
            // save
            question.save((err, question) => {
                if (err) {
                    return res.status(500).json({
                        message: 'Internal server error',
                        err: err
                    })
                }
                // send back the question
                res.status(200).json({
                    message: 'Question updated',
                    question: question
                })
            })
        })
    })
    
}

const updateAssessment = async (req, res, next) => {
    const user_id = res.locals.id
    const {title,questions_id} = req.body
    const assessment_id = req.params.id

    try {
        const assessment = await Assessment.findById(assessment_id)
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        if (assessment.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'Unauthorized'
            })
        }

        assessment.title = title ? title : assessment.title
        assessment.questions = questions_id ? questions_id : assessment.questions
        assessment.last_updated = new Date()
        assessment.status = 'pending'
        assessment.curator_id = null

        const session = await mongoose.startSession()

        session.withTransaction(async () => {
            // remove feedback with assessment_id = assessment_id
            await Feedback.deleteMany({assessment_id: assessment_id})
            // save
            await assessment.save()
        })

        return res.status(200).json({
            message: 'Assessment updated',
            assessment: assessment
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }
}

// DELETE

const deleteQuestion = async (req, res, next) => {
    const user_id = res.locals.id
    const {question_id,assessment_id} = req.params

    try{

        const assessment = await Assessment.findById(assessment_id)
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        if (assessment.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'Unauthorized'
            })
        }
        const question = await Question.findById(question_id)
        if (!question) {
            return res.status(404).json({
                message: 'Question not found'
            })
        }
        assessment.questions = assessment.questions.filter(id => id !== question_id)

        const session = await mongoose.startSession()

        await session.withTransaction(async () => {
            await assessment.save()
            await question.remove()
        })

        // session.endSession()

        res.status(200).json({
            message: 'Question deleted',
            assessment: assessment,
            deleted_question: question
        })
    }catch(err) {
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }
}

const deleteAssessment = async (req, res, next) => {
    const user_id = res.locals.id
    const assessment_id = req.params.id

    try {
        const assessment = await Assessment.findById(assessment_id)
        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        if (assessment.contributor_id !== user_id) {
            return res.status(403).json({
                message: 'Unauthorized'
            })
        }

        const session = await mongoose.startSession()

        await session.withTransaction(async () => {
            await Question.deleteMany({_id: {$in: assessment.questions}})
            await assessment.remove()
        })

        // session.endSession()

        res.status(200).json({
            message: 'Assessment deleted',
            assessment: assessment
        })
    }catch (err) {
        return res.status(500).json({
            message: 'Internal server error',
            err: err
        })
    }
}


module.exports = {
    createQuestion,
    createAssessment,
    getAssessment,
    updateQuestion,
    updateAssessment,
    deleteQuestion,
    deleteAssessment
}