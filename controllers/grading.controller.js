const TakesCourse = require('../models/takescourse')
const TakesMaterial = require('../models/takesmaterial')
const TakesAssessment = require('../models/takesassessment')
const Assessment = require('../models/assessment')
const Question = require('../models/question')
const Material = require('../models/material')
const Course = require('../models/course')
const Chapter = require('../models/chapter')
const AssessmentCourse = require('../models/assessmentcourse')

const db = require('../db/conn')
const mongoose = db.getMongoose()
const conn = mongoose.connection

const accomplishMaterial = async (req, res, next) => {
    const user_id = res.locals.id
    const {material_id,course_id} = req.params
    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message: 'Course not found'
            })
        }

        const chapters_id = course.chapters
        const chapters_in_course = await Chapter.find({_id: {$in: chapters_id}})
        if (!chapters_in_course) {
            return res.status(404).json({
                message: 'Chapters not found'
            })
        }

        var found = false
        chapters_in_course.forEach(chapter => {
            if (chapter.materials.includes(material_id)) {
                found = true
            }
        })

        if (!found) {
            return res.status(404).json({
                message: 'Material not found'
            })
        }

        const takescourse = await TakesCourse.findOne({user_id, course_id})
        if (!takescourse) {
            return res.status(404).json({
                message: 'User does not take this course'
            })
        }

        req.body.takescourse = takescourse

        const takesmaterial = await TakesMaterial.findOne({user_id, material_id})
        if (takesmaterial) {
            return res.status(400).json({
                message: 'User already accomplished this material'
            })
        }

        const newTakesMaterial = new TakesMaterial({ user_id: user_id, material_id: material_id })

        // await newTakesMaterial.save()

        res.response = {
            message: 'Material accomplished',
            takesmaterial: newTakesMaterial,
        }

        next()

    }catch(err){
        return res.status(400).json({
            message: 'Internal Server Error',
            err: err
        })
    }
}

/*
    example of answer request body:
    [
        { // if the question is multiple choice
            question_id: '5e9f8f8f8f8f8f8f8f8f8f8',
            answer_index: 0,
        },
        {
            question_id: '5e9f8f8f8f8f8f8f8f8f8f8',
            answer_index: 1,
        },
        { // if the question is checkbox
            question_id: '5e9f8f8f8f8f8f8f8f8f8f8',
            answer_index: [0, 1],
        },...
    ]
    */


const accomplishAssessment = async (req, res, next) => {
    const user_id = res.locals.id
    const {assessment_id,course_id} = req.params
    const {answers} = req.body
    try {
        // existence check
        const assessmentcourse = await AssessmentCourse.findOne({assessment_id: assessment_id,
            course_id: course_id})

        if (!assessmentcourse) {
            return res.status(404).json({
                message: 'Assessment/course not match'
            })
        }

        const takescourse = await TakesCourse.findOne({user_id: user_id, course_id: course_id})
        if (!takescourse) {
            return res.status(404).json({
                message: 'User does not take this course'
            })
        }
        req.body.takescourse = takescourse

        const takesassessment = await TakesAssessment.findOne({user_id: user_id,
             assessment_id: assessment_id})
        if (takesassessment) {
            return res.status(400).json({
                message: 'User already accomplished this assessment'
            })
        }

        // grading part
        const assessment = await Assessment.findById(assessment_id)

        if (!assessment) {
            return res.status(404).json({
                message: 'Assessment not found'
            })
        }

        if (assessment.status !== 'accepted') {
            return res.status(400).json({
                message: 'Assessment not accepted'
            })
        }

        const questions_id = assessment.questions

        const questions = await Question.find({_id: {$in: questions_id}})

        if (!questions) {
            return res.status(404).json({
                message: 'Questions not found'
            })
        }

        var true_answer = 0
        questions_id.forEach(question_id => {
            const c_question = questions.find(question => question._id.toString() === question_id)
            const c_solution = c_question.solution
            const c_type = c_question.type
            const c_answer = answers.find(answer => answer.question_id === question_id)

            if (!c_answer) {
                return res.status(400).json({
                    message: 'Answer not found'
                })
            }

            if (c_type === 'multiple-choice') {
                if (c_answer.answer_index === c_solution.index) {
                    true_answer++
                }
            } else if (c_type === 'multiple-answer') {
                const solution_index = c_solution.map(sol => sol.index)
                const answer_index = c_answer.answer_index
                if (solution_index.sort().join(',') === answer_index.sort().join(',')) {
                    true_answer++
                }
            }
        })

        const score = Math.round((true_answer / questions.length) * 100)

        const newTakesassessment = new TakesAssessment({
            user_id : user_id,
            assessment_id : assessment_id,
            score : score,
            answers : answers,
        })

        res.response = {
            message: 'Assessment accomplished',
            takesassessment: newTakesassessment,
        }

        // newTakesassessment.save()

        next()
    }catch(err) {
        console.log(err)
        return res.status(400).json({
            message: 'Internal Server Error',
            err: err
        })
    }
}

const updateProgress = async (req, res, next) => {
    const user_id = res.locals.id
    const {course_id} = req.params
    const takesassessment = res.response.takesassessment
    const takesmaterial = res.response.takesmaterial
    const takescourse = req.body.takescourse
    // assume course exist
    // assume takesassessment exist or takesmaterial exist
    if (takesassessment) {
        takescourse.assessment_done.push(takesassessment.assessment_id)
        takescourse.assessment_score.push(takesassessment.score)
    }

    if (takesmaterial) {
        takescourse.material_done.push(takesmaterial.material_id)
    }

    try {
        const course = await Course.findById(course_id)
        
        //chapter done
        const chapters_id = course.chapters
        const chapters = await Chapter.find({_id: {$in: chapters_id}})
        const materials_id = chapters.flatMap(chapter => chapter.materials)
        const materials = await Material.find({_id: {$in: materials_id}})

        // progress
        takescourse.progress = Math.round((takescourse.assessment_done.length  + takescourse.material_done.length) 
        / (course.total_assessment + course.total_material) * 100)

        var chapterDoneCount = 0
        chapters.forEach(chapter => {
            var isDone = true
            chapter.materials.forEach(material_id => {
                const material = materials.find(material => material._id.toString() === material_id)
                const material_type = material.type

                // if assessment then compare with material.link
                if (material_type === 'assessment') {
                    if (!takescourse.assessment_done.includes(material.link)) {
                        isDone = false
                    }
                }else {
                    if (!takescourse.material_done.includes(material_id)) {
                        isDone = false
                    }
                }
            })

            if (isDone) {
                chapterDoneCount++
            }
        })

        takescourse.chapter_done = chapterDoneCount
        
        if (takescourse.progress === 100) {
            takescourse.finish_date = new Date()
        }

        const session = await conn.startSession()

        session.withTransaction(async () => {
            // if exist res.response.takesassessment
            if (takesassessment) {
                await takesassessment.save()
            }
            // if exist res.response.takesmaterial
            if (takesmaterial) {
                await takesmaterial.save()
            }
            await takescourse.save()
        })

        // session.endSession()

        res.status(200).json(res.response)
    }catch(err) {
        console.log(err)
        return res.status(400).json({
            message: 'Internal Server Error',
            err: err
        })
    }
}

module.exports = {
    accomplishMaterial,
    accomplishAssessment,
    updateProgress
}