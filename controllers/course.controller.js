const Course = require('../models/course')
const Material = require('../models/material')
const Chapter = require('../models/chapter')
const User = require('../models/users')
const TakesCourse = require('../models/takescourse')
const Assessment = require('../models/assessment')
const Feedback = require('../models/feedback')

// Example of response
// export const materialDataDummy = [
//     {
//       chapter_id : 2314bj1234y91234,
//       chapter_title: 'Bagian 1: Pendahuluan',
//       content: [
//         {
//           type: 'material',
//           title: 'Apa itu Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//         {
//           type: 'video',
//           title: 'Bahasa Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//         {
//           type: 'assessment',
//           title: 'Uji Pemahaman',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//       ]
//     },
//     {
//       chapter_id : 2314bj1234y91234,
//       chapter_title: 'Bagian 2: Pengantar',
//       content: [
//         {
//           type: 'material',
//           title: 'Apa itu Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//         {
//           type: 'video',
//           title: 'Bahasa Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//       ]
//     },
//     {
//       chapter_id : 2314bj1234y91234,
//       chapter_title: 'Bagian 3: Pembuka',
//       content: [
//         {
//           type: 'material',
//           title: 'Apa itu Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//         {
//           type: 'video',
//           title: 'Bahasa Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//       ]
//     },
//     {
//       chapter_id : 2314bj1234y91234,
//       chapter_title: 'Bagian 4: Kesimpulan',
//       content: [
//         {
//           type: 'material',
//           title: 'Apa itu Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//         {
//           type: 'video',
//           title: 'Bahasa Assembly',
//           link: 'https://www.nesabamedia.com/pengertian-bahasa-assembly/'
//         },
//       ]
//     },
//   ]

const getCourseById = async (req,res,next) => {
    const user_id = res.locals.id
    const course_id = req.params.id
    var response = {}
    try {
        const course = await Course.findById(course_id)
        if (!course) {
            return res.status(404).json({
                message : 'Course not found'
            })
        }
        response.course = {
            ...course._doc,
        }
        const contributor = await User.findById(course.contributor_id)
        if (!contributor) {
            return res.status(404).json({
                message : 'User not found'
            })
        }
        response.course.contributor_name = contributor.full_name
        response.course.contributor_picture = contributor.profile_picture
        const chapters_id = course.chapters
        var chapters = await Chapter.find({_id : {$in : chapters_id}})
        if (!chapters) {
            return res.status(404).json({
                message : 'Chapter not found'
            })
        }
        const all_materials_id = chapters.flatMap(chapter => chapter.materials)
        const materials = await Material.find({_id : {$in : all_materials_id}})
        if (!materials) {
            return res.status(404).json({
                message : 'Material not found'
            })
        }
        const all_assessments_id = materials.filter(material => material.type === 'assessment').map(material => material.link)
        const all_assessments = await Assessment.find({_id : {$in : all_assessments_id}})
        if (!all_assessments) {
            return res.status(404).json({
                message : 'Assessment not found'
            })
        }
        // const all_assessments_id_declined = all_assessments.filter(assessment => assessment.status === 'declined').map(assessment => assessment._id.toString())
        const feedbacks = await Feedback.find({assessment_id : {$in : all_assessments_id}})
        response.data = chapters.map(chapter => {
            var currentData = {
                chapter_id: chapter._id,
                chapter_title: chapter.title,
            }

            const materials_id = chapter.materials
            var c_materials = materials.filter(material => materials_id.includes(material._id))
            c_materials = c_materials.map(material => {
                var currentMaterial = {
                    ...material._doc,
                }

                if (material.type === "assessment") {
                    const assessment = all_assessments.find(assessment => assessment._id.toString() === material.link)
                    currentMaterial.status = assessment.status
                    if (user_id === course.contributor_id) {
                        currentMaterial.feedback = feedbacks.find(feedback => feedback.assessment_id === assessment._id.toString())
                    }
                }

                return currentMaterial
            })

            currentData.content = c_materials
            return currentData
        })
        if (user_id) {
            response.is_contributor = course.contributor_id === user_id

            const takescourse = await TakesCourse.findOne({
                course_id : course_id,
                user_id : user_id
            })
            if (takescourse) {
                response.is_enrolled = true
                response.data.forEach(chapter => {
                    chapter.content.forEach(material => {
                        // if assessment put the score value
                        if (material.type === "assessment") {
                            const assessment_id = material.link
                            const assessment_idx = takescourse.assessment_done.indexOf(assessment_id)
                            if (assessment_idx !== -1) {
                                material.score = takescourse.assessment_score[assessment_idx]
                            }else {
                                material.score = null
                            }
                        }
                    })
                })
            }else {
                response.is_enrolled = false
                response.data.forEach(chapter => {
                    chapter.content.forEach(material => {
                        if (material.type === "assessment") {
                            material.score = null
                        }
                    })
                })
            }
        }else {
            response.is_contributor = false
            response.is_enrolled = false
            response.data.forEach(chapter => {
                chapter.content.forEach(material => {
                    if (material.type === "assessment") {
                        material.score = null
                    }
                })
            })
        }

        return res.status(200).json(response)
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err : err
        })
    }
}

const getPopularCourse = async (req,res,next) => {
    // take limit from params, if there is no then set limit to 3
    const limit = 3
    try {
        // get course with limit and sorted descended by taken_count
        const courses = await Course.find().sort({taken_count : -1}).limit(limit)
        return res.status(200).json({
            message: 'Popular courses',
            courses: courses
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err : err
        })
    }
}

const getPopularCourseTopic = async (req,res,next) => {
    const limit = 8
    try {
        // group courses by topic, then get the topic sorted by the total taken count descending
        const topics = await Course.aggregate([
            {
                $group : {
                    _id : '$topic',
                    total_taken_count : {$sum : '$taken_count'}
                }
            },
            {
                $sort : {
                    total_taken_count : -1
                }
            },
            {
                $limit : limit
            }
        ])
        if (!topics) {
            return res.status(404).json({
                message : 'Course not found'
            })
        }

        return res.status(200).json({
            message: 'Popular courses by topic',
            topics: topics
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err : err
        })
    }
}

const getCourseByTopic = async (req,res,next) => {
    const topic = req.params.topic
    const {page = 1, limit = 8} = req.params
    try {
        const courses = await Course.find({topic : topic}).skip((page - 1) * limit).limit(limit)
        if (!courses) {
            return res.status(404).json({
                message : 'Course not found'
            })
        }
        return res.status(200).json({
            message: 'Courses by topic',
            courses: courses
        })
    }catch(err) {
        return res.status(500).json({
            message : 'Internal server error',
            err : err
        })
    }
}


module.exports = {getCourseById,getPopularCourse,getPopularCourseTopic,getCourseByTopic}