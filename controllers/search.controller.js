const Course = require('../models/course')

// Get all the courses
const getCourses = (req, res, next) => {
    Course.find({}, (err, courses) => {
        if (err) {
            res.status(500).send({
                message: 'Internal server error',
                err: err
            })
            return
        }

        res.status(200).send(courses)
    })
}

// Get the array by page (16 item per page)
// For the moment, before using cache
const pagination = (array, page) => {
    const start = (page - 1) * 8
    const end = page * 8
    return array.slice(start, end)
}

// Get courses with the query using regex (the title)
const getCoursesByQueryRegex = (req, res, next) => {
    const query = req.params.query
    const page = req.params.page
    Course.find({
        title: {
            $regex: query,
            $options: 'i'
        }
    }, (err, courses) => {
        if (err) {
            res.status(500).send({
                message: 'Internal server error',
                err: err
            })
            return
        }

        res.status(200).send(pagination(courses, page))
    })
}


// Cosine similarity function for searching
const cosineSimilarity = (a, b) => {
    const dotProduct = a.reduce((dot, x, i) => dot + x * b[i], 0)
    const magnitudeA = Math.sqrt(a.reduce((sum, x) => sum + x * x, 0))
    const magnitudeB = Math.sqrt(b.reduce((sum, x) => sum + x * x, 0))
    if (magnitudeA === 0 || magnitudeB === 0) {
        return 0
    }
    return dotProduct / (magnitudeA * magnitudeB)
}

// Get courses with the query using cosine similarity sorted from title and desc
const getCoursesByQueryCosine = (req, res, next) => {
    const query = req.params.query.trim()
    const page = req.params.page

    Course.find({}, (err, courses) => {
        if (err) {
            res.status(500).send({
                message: 'Internal server error',
                err: err
            })
            return
        }

        // Get the title and desc of each course
        let titles = []
        let descs = []
        let topic = []
        courses.forEach((course) => {
            titles.push(course.title.toLowerCase())
            descs.push(course.description.toLowerCase())
            topic.push(course.topic.toLowerCase())
        })

        // Get the query vector by taking the count of each words in the query as key value pair
        let queryVector = {}
        query.split(' ').forEach((word_) => {
            const word = word_.toLowerCase()
            if (queryVector[word]) {
                queryVector[word] += 1
            } else {
                queryVector[word] = 1
            }
        })

        // Get the cosine similarity for each course
        let cosineSims = []
        for (let i = 0; i < titles.length; i++) {
            let titleVector = {}
            let descVector = {}
            let topicVector = {}

            query.split(' ').forEach((word_) => {
                const word = word_.toLowerCase()
                if (!titleVector[word]) {
                    titleVector[word] = 0
                }

                if (!descVector[word]) {
                    descVector[word] = 0
                }

                if (!topicVector[word]) {
                    topicVector[word] = 0
                }

                if (titles[i].toLowerCase().includes(word)) {
                    titleVector[word] += 1
                }

                if (descs[i].toLowerCase().includes(word)) {
                    descVector[word] += 1
                }

                if (topic[i].toLowerCase().includes(word)) {
                    topicVector[word] += 1
                }
            })

            const cosSimTitle = cosineSimilarity(Object.values(titleVector), Object.values(queryVector))
            const cosSimDesc = cosineSimilarity(Object.values(descVector), Object.values(queryVector))
            const cosSimTopic = cosineSimilarity(Object.values(topicVector), Object.values(queryVector))
            const cosineSimRes = 0.45*cosSimTitle + 0.15*cosSimDesc + 0.4*cosSimTopic
            cosineSims.push(cosineSimRes)
        }

        // Sort the courses
        let sortedCourses = []
        for (let i = 0; i < cosineSims.length; i++) {
            let maxIndex = cosineSims.indexOf(Math.max(...cosineSims))
            if (cosineSims[maxIndex] > 0) {
                sortedCourses.push(courses[maxIndex])
            }
            cosineSims[maxIndex] = -1
        }
        const response = {
            count: sortedCourses.length,
            data: pagination(sortedCourses, page),
        }
        res.status(200).send(response)
    })
}

module.exports = {
    getCourses,
    getCoursesByQueryRegex,
    getCoursesByQueryCosine
}