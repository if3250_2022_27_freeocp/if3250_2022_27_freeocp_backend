const express = require('express')
const curatorController = require('../controllers/curator.controller')
const router = express.Router()

router.put('/become-a-curator', curatorController.becomeACurator)

router.get('/pendingandall', curatorController.getPendingAndAll)

router.get('/all-pending/:page', curatorController.getAllPendingAssessments)

router.put('/review/:assessment_id', curatorController.reviewAssessment)

router.put('/accept/:assessment_id', curatorController.acceptAssessment)

router.put('/decline/:assessment_id', curatorController.declineAssessment)

router.get('/assessments/review/:page', curatorController.getAssessmentInReviewByCuratorId)

router.get('/assessments/accept/:page', curatorController.getAssessmentAcceptedByCuratorId)

router.get('/assessments/decline/:page', curatorController.getAssessmentDeclinedByCuratorId)

router.get('/assessments/all/:page', curatorController.getAssessmentByCuratorId)

module.exports = router