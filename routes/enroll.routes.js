const express = require('express')
const enrollController = require('../controllers/enroll.controller')
const router = express.Router()

router.post('/:course_id', enrollController.enrollCourse)

module.exports = router