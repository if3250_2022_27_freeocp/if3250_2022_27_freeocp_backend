const express = require('express');
// const searchController = require('../controllers/search.controller');
const uploadImageController = require('../controllers/upload.image.controller');
const router = express.Router();

// https://stackoverflow.com/questions/39589022/node-js-multer-and-req-body-empty

router.post('/profpic',uploadImageController.profpicUpload, uploadImageController.changeProfpicUrl);

router.post('/coursepic', uploadImageController.coursepicUpload, uploadImageController.changeCoursepicUrl);

router.put('/remove/profpic', uploadImageController.removeProfpic);

router.put('/remove/coursepic', uploadImageController.removeCoursepic);

module.exports = router;