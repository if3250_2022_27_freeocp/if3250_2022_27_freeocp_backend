const express = require('express')
const gradingController = require('../controllers/grading.controller')
const router = express.Router()

// accomplish material
router.post('/:course_id/material/:material_id', gradingController.accomplishMaterial,
gradingController.updateProgress)

// accomplish assessment
router.post('/:course_id/assessment/:assessment_id', gradingController.accomplishAssessment,
gradingController.updateProgress)

module.exports = router