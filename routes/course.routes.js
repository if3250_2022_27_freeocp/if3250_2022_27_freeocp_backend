const express = require('express')
const courseController = require('../controllers/course.controller')
const router = express.Router()


router.get('/popular-topic', courseController.getPopularCourseTopic)

router.get('/popular', courseController.getPopularCourse)

router.get('/:id', courseController.getCourseById)

router.get('/topic/:topic/:page', courseController.getCourseByTopic)

module.exports = router