const express = require('express')
const assessmentController = require('../controllers/assessment.controller')
const router = express.Router()

// CRUD

// CREATE
router.post('/create', assessmentController.createAssessment)

router.post('/question/create', assessmentController.createQuestion)

// READ
router.get('/:id', assessmentController.getAssessment)

// UPDATE
router.put('/:id/update', assessmentController.updateAssessment)

router.put('/:assessment_id/question/:question_id/update', assessmentController.updateQuestion)

// DELETE
router.delete('/:id/delete', assessmentController.deleteAssessment)

router.delete('/:assessment_id/question/:question_id/delete', assessmentController.deleteQuestion)

module.exports = router