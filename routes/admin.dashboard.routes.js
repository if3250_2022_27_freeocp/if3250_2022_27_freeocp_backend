const express = require('express')
const adminDashboardController = require('../controllers/admin.dashboard.controller')
const router = express.Router()

router.get('/all/:page', adminDashboardController.getAllCuratorRegistrations)

router.get('/pending/:page', adminDashboardController.getPendingCuratorRegistrations)

router.get('/not-pending/:page', adminDashboardController.getNotPendingCuratorRegistrations)

router.get('/approved/:page', adminDashboardController.getApprovedCuratorRegistrations)

router.get('/rejected/:page', adminDashboardController.getRejectedCuratorRegistrations)

router.put('/approve/:curator_registration_id', adminDashboardController.approveCuratorRegistration)

router.put('/reject/:curator_registration_id', adminDashboardController.rejectCuratorRegistration)

module.exports = router