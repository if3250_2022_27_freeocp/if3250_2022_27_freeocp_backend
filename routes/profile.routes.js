const express = require('express')
const profileControllers = require('../controllers/profile.controller')
const router = express.Router()

// Get username in params
router.get('/', profileControllers.getProfile)

// Post update profile
router.put('/', profileControllers.updateProfile)

// Post change password
router.put('/change-password', profileControllers.changePassword)

// Get course progress
router.get('/courses/:page', profileControllers.getCoursesProgress)

// Get course published
router.get('/courses/published/:page', profileControllers.getCoursesPublished)

module.exports = router