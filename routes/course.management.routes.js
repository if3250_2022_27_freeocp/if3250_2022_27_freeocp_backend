const express = require('express')
const courseManagementController = require('../controllers/course.management.controllers')
const router = express.Router()

// CREATE

router.post('/course/create', courseManagementController.createCourse)

router.post('/chapter/create', courseManagementController.createChapter)

router.post('/material/create', courseManagementController.createMaterial)

// UPDATE

router.put('/course/:course_id/update', courseManagementController.updateCourse)

router.put('/course/:course_id/chapter/:chapter_id/update', courseManagementController.updateChapter)

router.put('/course/:course_id/chapter/:chapter_id/material/:material_id/update', courseManagementController.updateMaterial)

router.put('/material/:material_id/update-link', courseManagementController.editLinkMaterial)

router.put('/course/:course_id/chapter/:chapter_id/add-assessment', courseManagementController.addAssessmentToChapter)

// DELETE

router.delete('/course/:course_id/delete', courseManagementController.deleteCourse)

router.delete('/course/:course_id/chapter/:chapter_id/delete', courseManagementController.deleteChapter)

router.delete('/course/:course_id/chapter/:chapter_id/material/:material_id/delete', courseManagementController.deleteMaterial)

module.exports = router