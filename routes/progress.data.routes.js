const express = require('express')
// const profileControllers = require('../controllers/profile.controller')
const progressDataControllers = require('../controllers/progress.data.controller')
const router = express.Router()

router.get('/:course_id', progressDataControllers.getProgress)

module.exports = router