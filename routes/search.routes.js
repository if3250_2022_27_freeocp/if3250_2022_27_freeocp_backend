const express = require('express');
const searchController = require('../controllers/search.controller');
const router = express.Router();

// get all courses
router.get('/', searchController.getCourses);

// get courses with the query using regex (the title)
router.get('/regex/:query/:page', searchController.getCoursesByQueryRegex);

// get courses with the query using cosine similarity sorted from title and desc
router.get('/:query/:page', searchController.getCoursesByQueryCosine);

module.exports = router;