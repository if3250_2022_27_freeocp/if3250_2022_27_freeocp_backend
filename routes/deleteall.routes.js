const express = require('express')
const deleteController = require('../controllers/delete.controller')
const router = express.Router()

// delete
router.delete('/', deleteController.deleteAll)

module.exports = router