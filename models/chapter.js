const db = require('../db/conn')
const mongoose = db.getMongoose()

const chapterSchema = new mongoose.Schema({
    title : String,
    materials : [String],
});

const Chapter = mongoose.model('chapters', chapterSchema)

module.exports = Chapter