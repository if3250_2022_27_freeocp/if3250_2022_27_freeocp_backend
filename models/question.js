const db = require('../db/conn')
const mongoose = db.getMongoose()

const questionSchema = new mongoose.Schema({
    type : String,
    description : String,
    options : Array,
    solution : mongoose.Schema.Types.Mixed,
    created_date : Date,
    last_updated : Date,
});

const Question = mongoose.model('questions', questionSchema);

module.exports = Question