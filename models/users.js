const db = require('../db/conn')
const mongoose = db.getMongoose()

const userSchema = new mongoose.Schema({
		email: String,
    password: String,
		full_name: String,
		date_of_birth: Date,
		is_curator: Boolean,
		profile_picture: String,
		institution: String,
		description: String,
		is_admin: Boolean,
});

const User = mongoose.model('users', userSchema);

module.exports = User