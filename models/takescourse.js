const db = require('../db/conn')
const mongoose = db.getMongoose()


const takesCourseSchema = new mongoose.Schema({
    user_id : String,
    course_id : String,
    material_done : [String],
    assessment_done : [String],
    assessment_score : [Number],
    chapter_done : Number,
    start_date : Date,
    finish_date : Date,
    progress : Number,
});

const TakesCourse = mongoose.model('takescourses', takesCourseSchema);

module.exports = TakesCourse