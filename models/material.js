const db = require('../db/conn')
const mongoose = db.getMongoose()

const materialSchema = new mongoose.Schema({
    title : String,
    type : String,
    link : String,
});

const Material = mongoose.model('materials', materialSchema);

module.exports = Material