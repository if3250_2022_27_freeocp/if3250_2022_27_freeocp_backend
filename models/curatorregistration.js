const db = require('../db/conn')
const mongoose = db.getMongoose()

const curatorRegistration = new mongoose.Schema({
    user_id : String,
    status : String,
    submission_date : Date,
});

const CuratorRegistration = mongoose.model('curator_registrations', curatorRegistration);

module.exports = CuratorRegistration