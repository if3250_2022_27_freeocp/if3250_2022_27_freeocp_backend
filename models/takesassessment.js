const db = require('../db/conn')
const mongoose = db.getMongoose()


const takesAssessmentSchema = new mongoose.Schema({
    user_id : String,
    assessment_id : String,
    score : Number,
    answers : Array,
});

const TakesAssessment = mongoose.model('takesassessments', takesAssessmentSchema);

module.exports = TakesAssessment