const db = require('../db/conn')
const mongoose = db.getMongoose()

const courseSchema = new mongoose.Schema({
    contributor_id : String,
    creator : String,
    title : String,
    topic : String,
    thumbnail : String,
    description : String,
    created_at : Date,
    last_updated : Date,
    taken_count : Number,
    total_material : Number,
    total_assessment : Number,
    total_chapter : Number,
    syllabus : Array,
    chapters : [String],
});

const Course = mongoose.model('courses', courseSchema);

module.exports = Course