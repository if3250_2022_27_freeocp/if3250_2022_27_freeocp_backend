const db = require('../db/conn')
const mongoose = db.getMongoose()


const takesMaterialSchema = new mongoose.Schema({
    user_id : String,
    material_id : String,
});

const TakesMaterial = mongoose.model('takesmaterials', takesMaterialSchema);

module.exports = TakesMaterial