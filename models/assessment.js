const db = require('../db/conn')
const mongoose = db.getMongoose()

const assessmentSchema = new mongoose.Schema({
    contributor_id : String,
    title : String,
    questions : Array,
    status: String,
    curator_id : String,
    created_date : Date,
    last_updated : Date,
});

const Assessment = mongoose.model('assessments', assessmentSchema);

module.exports = Assessment