const db = require('../db/conn')
const mongoose = db.getMongoose()

const feedbackSchema = new mongoose.Schema({
    assessment_id : String,
    feedback: String,
    curator_id: String,
})

const Feedback = mongoose.model('feedback_assessment', feedbackSchema);

module.exports = Feedback