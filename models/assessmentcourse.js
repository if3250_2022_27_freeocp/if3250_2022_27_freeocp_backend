const db = require('../db/conn')
const mongoose = db.getMongoose()

const assessmentCourseSchema = new mongoose.Schema({
    course_id : String,
    course_title : String,
    assessment_id : String,
});

const AssessmentCourse = mongoose.model('assessment_courses', assessmentCourseSchema);

module.exports = AssessmentCourse